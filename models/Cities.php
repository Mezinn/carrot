<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "ctities".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 *
 * @property Tours[] $tours
 */
class Cities extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'ctities';
    }

    public function behaviors() {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    public function rules() {
        return [
            [['name'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTours() {
        return $this->hasMany(Tours::className(), ['city_id' => 'id']);
    }

}
