<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $email_address
 * @property string $password_hash
 * @property string $created_at
 * @property string $updated_at
 * @property string $auth_key
 * @property string $access_token
 * @property string $name
 * @property string $surname
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface {

    public static function tableName() {
        return 'users';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ]
        ];
    }

    public function rules() {
        return [
            [['email_address', 'password_hash', 'auth_key', 'access_token', 'name', 'surname'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['email_address', 'name', 'surname'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 64],
            [['auth_key', 'access_token'], 'string', 'max' => 32],
            [['email_address'], 'unique'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'email_address' => 'Email Address',
            'password_hash' => 'Password Hash',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'name' => 'Name',
            'surname' => 'Surname',
        ];
    }

    public function getAuthKey(): string {
        return $this->auth_key;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey): bool {
        return $this->auth_key === $authKey;
    }

    public static function findIdentity($id): IdentityInterface {
        return self::findOne($id) ?: null;
    }

    public static function findIdentityByAccessToken($token, $type = null): IdentityInterface {
        return self::findOne(['access_token' => $token]) ?: null;
    }

}
