<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tours".
 *
 * @property int $id
 * @property int $city_id
 * @property int $tour_type_id
 * @property string $price
 * @property int $night_count
 * @property int $is_included_meal
 * @property int $is_travel_included
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $slug
 * @property int $is_active
 * @property string $description
 *
 * @property FavoriteTours[] $favoriteTours
 * @property Ctities $city
 * @property TourTypes $tourType
 * @property ViewedTours[] $viewedTours
 */
class Tours extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'tours';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    public function rules() {
        return [
            [['city_id', 'tour_type_id', 'price', 'night_count', 'is_included_meal', 'is_travel_included', 'name', 'is_active'], 'required'],
            [['city_id', 'tour_type_id', 'night_count', 'is_included_meal', 'is_travel_included', 'is_active'], 'integer'],
            [['price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['tour_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TourTypes::className(), 'targetAttribute' => ['tour_type_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'city_id' => 'Город',
            'tour_type_id' => 'Тип',
            'price' => 'Цена',
            'night_count' => 'Количество ночей',
            'is_included_meal' => 'Включено питание',
            'is_travel_included' => 'Включен проезд',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'name' => 'Название',
            'slug' => 'Slug',
            'is_active' => 'Активно',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteTours() {
        return $this->hasMany(FavoriteTours::className(), ['tour_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity() {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourType() {
        return $this->hasOne(TourTypes::className(), ['id' => 'tour_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewedTours() {
        return $this->hasMany(ViewedTours::className(), ['tour_id' => 'id']);
    }

}
