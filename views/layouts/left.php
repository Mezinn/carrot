<aside class="main-sidebar">

    <section class="sidebar">


        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        ['label' => 'Меню', 'options' => ['class' => 'header']],
                        ['label' => 'Города', 'icon' => 'file-code-o', 'url' => ['/manage/cities']],
                        ['label' => 'Типы', 'icon' => 'dashboard', 'url' => ['/manage/tour-types']],
                        ['label' => 'Туры', 'icon' => 'share', 'url' => ['/manage/tours']],
                        ['label' => 'Выход', 'icon' => 'sign-out', 'url' => ['/site/logout']],
                    ],
                ]
        )
        ?>

    </section>

</aside>
