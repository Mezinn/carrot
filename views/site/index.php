<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
?>


<?php
$form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]);
?>

<header class="header">
    <div class="form-wrapper">
        <?= $form->field($searchModel, 'name')->textInput(['class' => 'form-search-el', 'placeholder' => "Поиск"])->label(false) ?>
        <button class="icon-button" type="submit"><i class="fas fa-search"></i></button>
    </div>
    <?php if (!Yii::$app->user->isGuest): ?>
        <nav class="buttons-authorized-block">
            <?= Html::a('Избранное', ['/site/favorites'], ['class' => 'button text-center']) ?>
            <?= Html::a('Выход', ['/site/logout'], ['class' => 'button text-center']) ?>
        </nav>
    <?php endif; ?>
    <nav class="button-guest-block <?= Yii::$app->user->isGuest ? null : 'grid-none' ?>">
        <?php if (Yii::$app->user->isGuest): ?>
            <?= Html::a('Вход', ['/site/login'], ['class' => 'button text-center']) ?>
            <?= Html::a('Регистрация', ['/site/register'], ['class' => 'button text-center']) ?>
        <?php endif; ?>
    </nav>
</header>

<section class="ads">

    <a class="announcement" <?= !Yii::$app->user->isGuest ? 'style="margin-top: -60px;"' : '' ?>>

        <div class="main-inf">
            <div>
                <div class="form-hint">Страна</div>
                <?= $form->field($searchModel, 'city_id')->dropDownList(Yii::$app->citiesHelper->getList(), ['class' => 'information-field', 'onchange' => 'submit()'])->label(false) ?></div>
            <div>
                <div class="form-hint">Тип тура</div>
                <?= $form->field($searchModel, 'tour_type_id')->dropDownList(Yii::$app->tourTypesHelper->getList(), ['class' => 'information-field', 'onchange' => 'submit()'])->label(false) ?></div>
            <div>
                <div class="form-hint">Максимальная цена</div>
                <?= $form->field($searchModel, 'price')->textInput(['class' => 'information-field', 'onchange' => 'submit()'])->label(false) ?></div>
            <div>
                <div class="form-hint">Количество ночей</div>
                <?= $form->field($searchModel, 'night_count')->textInput(['class' => 'information-field', 'onchange' => 'submit()'])->label(false) ?></div>

        </div>

        <div class="add-inf">

            <div class="center">
                <?= $form->field($searchModel, 'is_included_meal')->checkbox(['class' => 'information-field', 'label' => null, 'onchange' => 'submit()']) ?>
                <span>Питание включено в стоимость</span>
            </div>

            <div class="center">
                <?= $form->field($searchModel, 'is_travel_included')->checkbox(['class' => 'information-field', 'onchange' => 'submit()', 'label' => null]) ?>
                <span>Проезд включен в стоимость</span>
            </div>
        </div>



    </a>
</section>

<?php ActiveForm::end(); ?>

<?php Pjax::begin(['id' => 'tours']) ?>
<section class="tours">
    <?= ListView::widget(['dataProvider' => $dataProvider, 'itemView' => '_template']) ?>
</section>
<?php Pjax::end() ?>
<script>
    function toggle_favorite(event) {
        event.preventDefault();
        $.ajax({
            url: '/site/toggle-favorite',
            data: {id: event.target.dataset.id},
            success: function (data, textStatus, jqXHR) {
                $.pjax.reload({container: '#tours'});
            }
        });
    }
</script>