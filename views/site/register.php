<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <?php
    $form = ActiveForm::begin();
    ?>
    <section class="login pd-25px">
        <p class="login-head">Регистрация</p>

        <?= $form->field($model, 'name', ['options' => ['class' => 'w-100 mt-15px']])->textInput(['autofocus' => true, 'class' => 'login-form-el', 'placeholder' => 'Имя'])->label(false) ?>

        <?= $form->field($model, 'surname', ['options' => ['class' => 'w-100 mt-15px']])->textInput(['autofocus' => true, 'class' => 'login-form-el', 'placeholder' => 'Фамилия'])->label(false) ?>

        <?= $form->field($model, 'email_address', ['options' => ['class' => 'w-100 mt-15px']])->textInput(['autofocus' => true, 'class' => 'login-form-el', 'placeholder' => 'Email адресс'])->label(false) ?>


        <?= $form->field($model, 'password', ['options' => ['class' => 'w-100 mt-15px']])->passwordInput(['class' => 'login-form-el', 'placeholder' => 'Пароль'])->label(false) ?>

        <?= $form->field($model, 'confirm_password', ['options' => ['class' => 'w-100 mt-15px']])->passwordInput(['class' => 'login-form-el', 'placeholder' => 'Повторите пароль'])->label(false) ?>


        <?= Html::submitButton('Зарегистрироваться', ['class' => 'button w-100 mt-15px']) ?>
        <?= Html::a('Войти', ['site/login'], ['class' => 'w-100 mt-15px text-center']) ?>

        <?php ActiveForm::end(); ?>
    </section>

</div>
