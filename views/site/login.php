<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <?php
    $form = ActiveForm::begin();
    ?>
    <section class="login pd-25px">
        <p class="login-head">Вход</p>


        <?= $form->field($model, 'email_address', ['options' => ['class' => 'w-100 mt-15px']])->textInput(['autofocus' => true, 'class' => 'login-form-el', 'placeholder' => 'Email адресс'])->label(false) ?>


        <?= $form->field($model, 'password', ['options' => ['class' => 'w-100 mt-15px']])->passwordInput(['class' => 'login-form-el', 'placeholder' => 'Пароль'])->label(false) ?>


        <?= Html::submitButton('Войти', ['class' => 'button w-100 mt-15px']) ?>
        <?= Html::a('Зарегистрироваться', ['site/register'], ['class' => 'w-100 mt-15px text-center']) ?>

        <?php ActiveForm::end(); ?>
    </section>

</div>
