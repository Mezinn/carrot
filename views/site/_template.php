<div class="announcement tour">
    <div class="tour__name">
        <span>Название: <?= $model->name ?></span>
        <span><?= $model->is_included_meal ? 'Включено питание' : '' ?></span>
    </div>
    <div class="tour__price">
        <span>Цена: <?= $model->price ?></span>
        <span><?= $model->is_travel_included ? 'Включен проезд' : '' ?></span>
    </div>
    <div class="tour__city">Город: <?= $model->city->name ?></div>
    <div class="tour__type">Тип: <?= $model->tourType->name ?></div>
    <div class="tour__night">Количество ночей: <?= $model->night_count ?></div>
    <div class="tour_button add_favorite" data-id="<?= $model->id ?>" onclick="toggle_favorite(event)"><?= Yii::$app->tourHelper->isFavorite($model->id) ? 'Удалить' : 'В избранное' ?></div>
</div>
