<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
?>



<header class="header">
    <?php
    $form = ActiveForm::begin([
                'action' => ['/site/index'],
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1
                ],
    ]);
    ?>    

    <div class="form-wrapper">
        <?= $form->field(new \app\models\search\ToursOuterSearch(), 'name')->textInput(['class' => 'form-search-el', 'placeholder' => "Поиск"])->label(false) ?>
        <button class="icon-button" type="submit"><i class="fas fa-search"></i></button>
    </div>
    <?php ActiveForm::end() ?>
    <?php if (!Yii::$app->user->isGuest): ?>
        <nav class="buttons-authorized-block">
            <?= Html::a('Главная', ['/site/index'], ['class' => 'button text-center']) ?>
            <?= Html::a('Выход', ['/site/logout'], ['class' => 'button text-center']) ?>
        </nav>
    <?php endif; ?>
    <nav class="button-guest-block <?= Yii::$app->user->isGuest ? null : 'grid-none' ?>">
        <?php if (Yii::$app->user->isGuest): ?>
            <?= Html::a('Вход', ['/site/login'], ['class' => 'button text-center']) ?>
            <?= Html::a('Регистрация', ['/site/register'], ['class' => 'button text-center']) ?>
        <?php endif; ?>
    </nav>
</header>

<?php Pjax::begin(['id' => 'favorites']) ?>
<section class="tours">
    <?= ListView::widget(['dataProvider' => $dataProvider, 'itemView' => '_template']) ?>
</section>
<?php Pjax::end() ?>
<script>
    function toggle_favorite(event) {
        event.preventDefault();
        $.ajax({
            url: '/site/toggle-favorite',
            data: {id: event.target.dataset.id},
            success: function (data, textStatus, jqXHR) {
                $.pjax.reload({container: '#favorites'});
            }
        });
    }
</script>