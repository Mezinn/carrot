<?php

//use Yii;
use yii\db\Migration;

/**
 * Class m181121_195437_init_admin
 */
class m181121_195437_init_admin extends Migration {

    public function up() {
        Yii::$app->userHelper->createNewAdmin('admin', '12345678');
    }

}
