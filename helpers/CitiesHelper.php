<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers;

use app\models\Cities;
use yii\helpers\ArrayHelper;

/**
 * Description of CitiesHelper
 *
 * @author mezinn
 */
class CitiesHelper extends \yii\base\Component {

    public function getList() {
        return ArrayHelper::map(Cities::find()->all(), 'id', 'name');
    }

}
