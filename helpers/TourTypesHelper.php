<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers;

use yii\helpers\ArrayHelper;
use app\models\TourTypes;

/**
 * Description of TourTypesHelper
 *
 * @author mezinn
 */
class TourTypesHelper extends \yii\base\Component {

    public function getList() {
        return ArrayHelper::map(TourTypes::find()->all(), 'id', 'name');
    }

}
