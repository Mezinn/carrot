<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers\users;

use Yii;
use Exception;
use app\models\Users;
use app\forms\users\UserForm;
use yii\web\NotFoundHttpException;
use app\forms\users\RegisterForm;
use app\forms\users\LoginForm;

/**
 * Description of UserHelper
 *
 * @author mezinn
 */
class UserHelper extends \yii\base\Component {

    public function createNew(UserForm $form) {
        if (!$form->validate()) {
            return false;
        }
        return Yii::$app->getDb()->transaction(function()use($form) {
                    $user = new Users([
                        'name' => $form->getName(),
                        'surname' => $form->getSurname(),
                        'email_address' => $form->getEmailAddress(),
                        'password_hash' => Yii::$app->security->generatePasswordHash($form->getPassword()),
                        'auth_key' => Yii::$app->security->generateRandomString(),
                        'access_token' => Yii::$app->security->generateRandomString(),
                    ]);
                    return $user->save() and Yii::$app->authManager->assign(Yii::$app->authManager->getRole(USER), $user->id);
                });
    }

    public function createNewAdmin($username, $password) {
        return Yii::$app->getDb()->transaction(function()use($username, $password) {
                    $user = new Users([
                        'name' => 'admin',
                        'surname' => 'admin',
                        'email_address' => $username,
                        'password_hash' => Yii::$app->security->generatePasswordHash($password),
                        'auth_key' => Yii::$app->security->generateRandomString(),
                        'access_token' => Yii::$app->security->generateRandomString(),
                    ]);
                    return $user->save() and Yii::$app->authManager->assign(Yii::$app->authManager->getRole(ADMIN), $user->id);
                });
    }

    public function login(UserForm $form) {
        try {
            if (( $model = $this->findIdentityByEmailAddress($form->getEmailAddress())) and Yii::$app->security->validatePassword($form->getPassword(), $model->password_hash)) {
                Yii::$app->user->login($model);
                return true;
            }
        } catch (Exception $ex) {
            
        }
        $form->pushError('Incorrect username or password.');
        return false;
    }

    public function findIdentityById(int $id) {
        if (($model = Users::findOne($id))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

    public function findIdentityByEmailAddress(string $email_address) {
        if (($model = Users::findOne(['email_address' => $email_address]))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

    public function findIdentityByAccessToken(string $access_token) {
        if (($model = Users::findOne(['access_token' => $access_token]))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

    public function getUserRegisterForm() {
        return new RegisterForm();
    }

    public function getLoginForm() {
        return new LoginForm();
    }

}
