<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers\views;

/**
 * Description of ViewManager
 *
 * @author mezinn
 */
class ViewManager extends \yii\base\Component {

    private $bodyClass;

    public function setBodyClass(string $class) {
        $this->bodyClass = $class;
        return $this;
    }

    public function getBodyClass() {
        return $this->bodyClass;
    }

}
