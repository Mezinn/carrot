<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helpers;

use Yii;
use app\models\FavoriteTours;

/**
 * Description of TourHelper
 *
 * @author mezinn
 */
class TourHelper extends \yii\base\Component {

    public function toggleFavorite($id) {
        if (($model = FavoriteTours::findOne(['user_id' => Yii::$app->user->id, 'tour_id' => $id]))) {
            return $model->delete();
        } else {
            return (new FavoriteTours(['user_id' => Yii::$app->user->id, 'tour_id' => $id]))->save();
        }
    }

    public function isFavorite($id) {
        return FavoriteTours::findOne(['user_id' => Yii::$app->user->id, 'tour_id' => $id]);
    }

}
