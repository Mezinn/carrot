<?php


namespace app\forms\users;

use yii\base\Model;
use app\models\Users;

/**
 * Description of RegisterForm
 *
 * @author mezinn
 */
class RegisterForm extends Model implements UserForm {

    public $email_address;
    public $password;
    public $confirm_password;
    public $name;
    public $surname;

    public function rules() {
        return [
            [['email_address', 'password', 'confirm_password', 'surname', 'name'], 'required'],
            [['email_address', 'name', 'surname', 'password', 'confirm_password'], 'string', 'max' => 255],
            [['confirm_password'], 'compare', 'compareAttribute' => 'password'],
            [['email_address'], 'email'],
            [['email_address'], 'unique', 'targetClass' => Users::className()],
        ];
    }

    public function getEmailAddress() {
        return $this->email_address;
    }

    public function getName() {
        return $this->name;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSurname() {
        return $this->surname;
    }

    public function pushError($error) {
        
    }

}
