<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\forms\users;

/**
 * Description of UserForm
 *
 * @author mezinn
 */
interface UserForm {

    public function getEmailAddress();

    public function getPassword();

    public function pushError($error);

    public function getName();

    public function getSurname();
}
