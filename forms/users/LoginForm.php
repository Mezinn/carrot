<?php

namespace app\forms\users;

use yii\base\Model;

/**
 * Description of LoginForm
 *
 * @author mezinn
 */
class LoginForm extends Model implements UserForm {

    public $email_address;
    public $password;

    public function rules() {
        return [
            [['email_address', 'password'], 'required'],
            [['email_address', 'password'], 'string', 'max' => 255],
        ];
    }

    public function pushError($error) {
        $this->addError('password', $error);
    }

    public function getEmailAddress() {
        return $this->email_address;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getName() {
        return '';
    }

    public function getSurname() {
        return '';
    }

}
